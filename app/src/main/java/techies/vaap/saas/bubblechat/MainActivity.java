package techies.vaap.saas.bubblechat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    ArrayList<String> message;
    ArrayList<String> user;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.bubble_main);
        message = new ArrayList<>();
        user = new ArrayList<>();


        message.add("Hii");
        message.add("How R u ?");
        message.add("How R u ? Be careful with this message. It contains content that's typically used to steal personal information. Learn more\n" +
                "Report this suspicious message   Ignore, I trust this message\n");
        message.add("");
        message.add("Hiiii");
        user.add("me");
        user.add("he");
        user.add("me");
        user.add("img");

        user.add("he");
        user.add("vid");
        message.add("");

        CustomList adapter = new CustomList(MainActivity.this, message,user);
        listView.setAdapter(adapter);
    }
}
