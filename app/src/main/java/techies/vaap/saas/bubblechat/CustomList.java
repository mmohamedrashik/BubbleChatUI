package techies.vaap.saas.bubblechat;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.net.Uri;
import android.support.annotation.RequiresPermission;
import android.support.v4.media.session.IMediaControllerCallback;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ActionMenuView;
import android.widget.ArrayAdapter;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;

import it.sephiroth.android.library.picasso.Picasso;


public class CustomList extends ArrayAdapter<String>{


    private final Activity context;
    private final ArrayList<String> name,user;
    String cuser = "";

    public CustomList(Activity context,
                      ArrayList<String> name,ArrayList<String> user) {
        super(context, R.layout.bubble_single, name);
        this.context = context;
        this.name = name;
        this.user = user;


    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.bubble_single, null, true);


        TextView names = (TextView) rowView.findViewById(R.id.chat1);
        ImageView imageView = (ImageView)rowView.findViewById(R.id.imgholder);
        VideoView videoView = (VideoView)rowView.findViewById(R.id.videov);

       cuser =user.get(position);
        if (cuser.equals("me")) {
            imageView.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
            names.setVisibility(View.VISIBLE);

            names.setBackgroundResource(R.drawable.bubble_green);

            names.setGravity(Gravity.LEFT);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) names.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            names.setLayoutParams(params);
            names.setText(name.get(position));
        }else if(cuser.equals("he"))
        {
            imageView.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
            names.setVisibility(View.VISIBLE);
            names.setBackgroundResource(R.drawable.bubble_yellow);
            names.setGravity(Gravity.LEFT);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) names.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

           // names.setVisibility(View.GONE);
            names.setLayoutParams(params);
            names.setText(name.get(position));
        }
        else if(cuser.equals("img"))
        {
            names.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
            imageView.setBackgroundResource(R.drawable.bubble_green);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) names.getLayoutParams();
            Picasso.with(context).load("https://dizivizi.com/mbb/imgs/site/default_user.png").resize(250, 200).into(imageView);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            imageView.setLayoutParams(params);
        }else if(cuser.equals("vid"))
        {


        }



        return rowView;
    }
    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}